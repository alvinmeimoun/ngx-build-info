#!/usr/bin/env node

const fs     = require('fs'),
    exec     = require('child_process').execSync,
    files    = [],
    encoding = 'utf-8',
    gitData  = exec('git log -1 --pretty=format:%H\\|%ct\\|%an\\|%ce\\|%D\\|%B').toString().split('|'),
    values   = {
      buildTime:      Date.now(),
      gitHash:        gitData[0],
      gitHashShort:   gitData[0].substr(0, 8),
      gitTimestamp:   gitData[1],
      gitAuthorName:  gitData[2],
      gitAuthorEmail: gitData[3],
      gitBranch:      gitData[4].split(',')[0].substr(8),
      gitMessage:     gitData[5].replace(/\r?\n|\r/g, ""),
    };

// Determine file name regex
let fileExp = 'index\.html';
if (process.argv[2]) {
  if (process.argv[2] === 'main') {
    fileExp = 'main-.*\.js';
  } else if (process.argv[2] === 'all') {
    fileExp = '[\.html|\.js]';
  } else {
    fileExp = process.argv[2];
  }
}

// Find all files to perform find/replace
let exp = new RegExp(fileExp, 'g');
fs.readdirSync('./dist').forEach(d => {
    try {
        fs.readdirSync('./dist/' + d).forEach(f => {
            if (exp.test(f)) {
                files.push('./dist/' + d + '/' + f);
            }
        })
    } catch(e) {
      // Scanning for directories -- easier than testing each one
    }
});

// Loop through files and replace
files.forEach(f => {
  const replaced = Object.keys(values).reduce((contents, key) => contents.replace((new RegExp('\\${' + key + '}', 'g')), values[key]), fs.readFileSync(f, encoding));
  fs.writeFileSync(f, replaced, encoding);
});
